import tornado.web
from  tornado.escape import json_decode, json_encode
from sqlalchemy import desc
from sqlalchemy.orm import exc
import sqlite3
import hashlib
import uuid, os
from pdf2image import convert_from_path, convert_from_bytes
import pdb

import logging

# sql 
import models


PDF_DIR = 'files/pdf/'
PNG_DIR = 'files/png/'
LOG_FILENAME = 'err_level.log'
LOG_DIR = '/var/log/nice_pdf/{filename}'.format(filename=LOG_FILENAME)

logger = logging.getLogger()
logger.setLevel(logging.ERROR)
logging_handler = logging.FileHandler('./log/err_level.log') 
logger.addHandler(logging_handler)


def hash_string(value):
    """
    Создание хеш-функции. Для теста без salt
    """
    md5 = hashlib.md5()
    md5.update(value.encode('utf-8'))
    return md5.hexdigest()

class BaseHandler(tornado.web.RequestHandler):
    """
    Базовый обработчик
    """
    def get_current_user(self):
        """
        Получение текущего пользователя
        """
        try:
            # Надо кешировать по-хорошему
            email_cookie = self.get_secure_cookie('user')
            if not email_cookie:
                return None
            email = str(email_cookie, 'utf-8')
            user = models.session.query(models.User).filter(models.User.email==email).one()
            return user
        # No user could be found 
        except exc.NoResultFound as e:
            return None


class AuthenticationHandler(BaseHandler):
    """
    Обработка аутентификации
        -- method: метод аутентификации 
    """
    def post(self, method):
        if method == 'sign-in':
            email = self.get_argument("email")
            password = hash_string(self.get_argument("password"))
            try: 
                user = models.session.query(models.User).filter(models.User.email==email, models.User.password==password).one()
                self.set_secure_cookie("user", email)
            except exc.NoResultFound as e:
                self.render('templates/index.html', error='Неверная пара "Логин - пароль"')
            except sqlite3.IntegrityError as e: 
                logger.error(e)
        elif method == 'sign-up':
            email = self.get_argument("email")
            password = hash_string(self.get_argument("password"))
            try: 
                user = models.User(email=email, password=password)
                models.session.add(user)
                models.session.commit()
                self.set_secure_cookie("user", email)
            except sqlite3.IntegrityError as e: 
                self.render('templates/index.html', error='Пользователь с данным Email уже существует')
            except Exception as e:
                logger.error(e)
        elif method == 'sign-out':
            self.clear_cookie('user')
        self.redirect('/')


class FileHandler(BaseHandler):
    """
    Обработка аплодинга (загрузки на сервер) PDF-файлов
    """
    def post(self):
        try: 
            # Обработка данных файла и переменных
            file_upload = self.request.files['file-upload'][0]
            filename = file_upload['filename']
            extension = os.path.splitext(filename)[-1]
            local_name = str(uuid.uuid4()) + extension
            full_name = PDF_DIR + local_name
            
            # Сохранение в файл
            _file = open(full_name, 'wb')
            _file.write(file_upload['body'])
            _file.close()

            # Сохранение файла
            pdf_file = models.PdfFile(user=self.current_user, filename=filename, inner_name=local_name)
            models.session.add(pdf_file)
            models.session.commit()

            # Конвертация в PNG файлы 
            images = convert_from_path(full_name, output_folder=PNG_DIR, fmt='png')
            png_to_write = []
            for idx, image in enumerate(images):
                png_file = {
                    'filename': os.path.split(image.filename)[-1],
                    'pdf_file': pdf_file,
                    'page': idx, 
                }
                models.session.add(models.PngFile(**png_file))
                models.session.commit()
            models.session.commit()
            # В любой непонятной ситуации перенаправление на главную страницу ? 
            self.redirect('/')
        except Exception as e:
            logger.error(e)
            self.redirect('/')


class BaseDownloadHandler(BaseHandler):
    """
    Обработчик скачивания файлов
    """
    # Переопределить перед использованием наследования
    base_dir = None # Директория для хранения файлов

    def __init__(self, *args, **kwargs):
        # ToDo: ввести обход по словарю членов класса, если обязательных свойств станет больше 2
        if self.base_dir is None:
            raise NotImplementedError()
        super().__init__(*args, **kwargs)

    def get_instance(self):
        """
        * required
        Получение объекта файла
        """
        raise NotImplementedError()

    def get(self, filename):
        """
        Получение файла
            -- имя файла, по которому произовдится выборка. Должно быть уникальныым (хешируемым) среди всех файлов
        """
        user = self.current_user
        try: 
            params = {'filename': filename}
            instance = self.get_instance(**params)
        except: 
            self.redirect('/')
        filedir = os.path.abspath('' + self.base_dir)
        fullname = '{}/{}'.format(filedir, instance.inner_name)
        if not os.path.exists(fullname):
            self.redirect('/')
        self.set_header('Content-Type', 'application/force-download')
        self.set_header('Content-Disposition', 'attachment; filename=' + filename)    
        with open(fullname, "rb") as f:
            try:
                while True:
                    _buffer = f.read(4096)
                    if _buffer:
                        self.write(_buffer)
                    else:
                        f.close()
                        self.finish()
                        return
            except:
                raise HTTPError(404)
        raise HTTPError(500)



class DownloadPdfHandler(BaseDownloadHandler):
    """
    Обработчик скачивания PDF-файла
    """
    base_dir = PDF_DIR

    def get_instance(self, **kwargs):
        """
            -- filename
        """
        filename = kwargs['filename']
        return models.session.query(models.PdfFile).filter(models.PdfFile.filename == filename).one()


class DownloadPngHandler(BaseDownloadHandler):
    """
    Обработчик скачивания PNG-файла
    """
    base_dir = PNG_DIR

    def get_instance(self, **kwargs):
        """
            -- filename:  
        """
        filename = kwargs['filename']
        return models.session.query(models.PngFile).filter(models.PngFile.filename == filename).one()

class MainHandler(BaseHandler):
    """
    Основная страница
    """
    def get(self):
        user = self.current_user
        context = {}
        # Для неаутентифицированных пользователей - лендинг 
        if not user:
            self.render('templates/index.html')
        # Для остальных - основная функциональная страница 
        else:
            # Все PDF-файлы данного пользователя
            user_files = models.session.query(models.PdfFile).filter(models.PdfFile.user == self.current_user).order_by(desc(models.PdfFile.created)).all()
            context['user_files'] = user_files
            self.render('templates/nice_pdf.html', **context)


