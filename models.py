from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, SmallInteger
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, backref
from sqlalchemy import create_engine
import datetime

import pdb

Base = declarative_base()

class User(Base):
    """
    Пользователи
    """
    __tablename__ = 'user'

    ROLES = (
        (0, 'user'),
        (1, 'admin'),
    )
    id = Column(Integer, primary_key=True, unique=True)
    email = Column(String(50), nullable=False, unique=True)
    password = Column(String(50), nullable=False)
    role = Column(SmallInteger, nullable=True, default=0)
 
class PdfFile(Base):
    """
    PDF-файлы
    """
    __tablename__ = 'pdf_file'
    id = Column(Integer, primary_key=True)
    filename = Column(String(65))
    inner_name = Column(String(65))
    created = Column(DateTime, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship(User, backref=backref('pdf_file_list', lazy='dynamic'))


class PngFile(Base):
    """
    PNG-файлы, соответствующие PDF-файлам. Для простоты и демонстрации представлены в виде отдельной сущности вместо дочерних элементов сущности File
    """
    __tablename__ = 'png_file'
    id = Column(Integer, primary_key=True)
    filename = Column(String(65))
    page = Column(Integer)
    pdf_file_id = Column(Integer, ForeignKey('pdf_file.id'))
    pdf_file = relationship(PdfFile, backref=backref('png_file_list', lazy='dynamic'))

    @property
    def inner_name(self):
        return self.filename

engine = create_engine('sqlite:///nice_pdf.db')
Base.metadata.create_all(engine)

Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()

