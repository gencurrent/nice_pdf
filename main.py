""" 
    main.py 
    Artem Bulatov
"""
import tornado.ioloop
import handlers

def make_app():
    static_path_dir = './static/'
    return tornado.web.Application(
        [
            (r'/', handlers.MainHandler),
            (r'/auth/(?P<method>sign\-in|sign\-up|sign\-out)/$', handlers.AuthenticationHandler),
            (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': static_path_dir}),
            (r'/upload-file/$', handlers.FileHandler),
            (r'/download/pdf/(?P<filename>[0-9,a-z,A-Z,\.,\-]*)/$', handlers.DownloadPdfHandler),
            (r'/download/png/(?P<filename>[0-9,a-z,A-Z,\.,\-]*)/$', handlers.DownloadPngHandler),
        ], 
        cookie_secret="2aaFlNpra5yaQBba"
    )

if __name__ == "__main__":
    app = make_app()
    app.listen(8000, '127.0.0.1')
    tornado.ioloop.IOLoop.current().start()
